SHELL = /bin/bash

bash-completion:
	echo 'eval "$(_WEATHERD_COMPLETE=bash_source weatherd)"' >> ~/.bashrc

zsh-completion:
	echo 'eval "$(_WEATHERD_COMPLETE=zsh_source weatherd)"' >> ~/.zshrc

fish-completion:
	echo 'env _WEATHERD_COMPLETE=fish_source weatherd' > ~/.config/fish/completions/weatherd.fish

init-db:
	alembic upgrade head

launch:
	touch data/log/weatherd.log
	env | grep '^WEATHERD_' > /tmp/weatherd.env
	cat /tmp/weatherd.env etc/cron/crontab > /etc/cron.d/weatherd
	chmod 0644 /etc/cron.d/weatherd
	crontab /etc/cron.d/weatherd
	service cron reload
	service cron restart
	tail -f data/log/weatherd.log

cronjob:
	weatherd bulk --save everything 2>&1 >> data/log/weatherd.log
