.. _weatherd_readme:

########
weatherd
########

Installation
============

Package installation
--------------------

Install from the repo in dev mode: ``pip install -e '.[all]'``

Docker
------

1. Set up environment variables (example with ``direnv``)::

    cp envrc.example .envrc
    vim .envrc
    direnv allow

2. Build Docker image::

    docker build -t weatherd:latest .

3. Start containers using ``docker-compose``::

    docker-compose up

4. Migrate dababase::

    docker-compose run -T app make init-db


Usage
=====

For usage options run::

    weatherd --help

Global options are::

    λ weatherd --help

    Usage: weatherd [OPTIONS] COMMAND [ARGS]...

    Options:
      -v, --verbosity TEXT           Logging verbosity
      -d, --db-url TEXT              Database URL
      --accuweather-api-key TEXT     AccuWeather API key
      --openweathermap-api-key TEXT  OpenWeatherMap API key
      --help                         Show this message and exit.

    Commands:
      bulk
      fetch

Atomic fetch commands and options are under the ``fetch`` command group::

    λ weatherd fetch --help

    Usage: weatherd fetch [OPTIONS] COMMAND [ARGS]...

    Options:
      --save / --dry-run              Save results
      -y, --latitude FLOAT            Latitude
      -x, --longitude FLOAT           Longitude
      -p, --provider [accuweather.AccuWeather|openweathermap.OpenWeatherMap]
                                      Provider
      --help                          Show this message and exit.

    Commands:
      forecast-all     Fetch all forecasts.
      forecast-daily   Fetch daily forecasts.
      forecast-hourly  Fetch hourly forecasts.
      location-meta    Fetch location meta.

To see individual command usage directions do e.g.::

    λ weatherd fetch forecast-daily --help

    Usage: weatherd fetch forecast-daily [OPTIONS]

      Fetch daily forecasts.

    Options:
      -d, --duration DURATION
      --help                   Show this message and exit.

Bulk group commands allow fetching data in larger quantities::

    λ weatherd bulk --help

    Usage: weatherd bulk [OPTIONS] COMMAND [ARGS]...

    Options:
      --save / --dry-run  Save results
      --help              Show this message and exit.

    Commands:
      all-locations  Fetch forecasts for all locations.
      all-meta       Fetch all locations' meta.
      all-providers  Fetch location forecasts by all providers.
      everything     Fetch location forecasts by all providers.

For instance ``all-locations`` would fetch data for all known locations
by specified ``--provider``::

    λ weatherd bulk all-locations --help

    Usage: weatherd bulk all-locations [OPTIONS]

      Fetch forecasts for all locations.

    Options:
      -p, --provider [accuweather.AccuWeather|openweathermap.OpenWeatherMap]
                                      Provider
      --help                          Show this message and exit.

Examples
--------

1. Fetch and store ``OpenWeatherMap`` provider hourly forecast for 3 next hours
   at latitude 5.59 and longitude -0.19::

    weatherd fetch -y 5.59 -x -0.19 -p openweathermap.OpenWeatherMap --save forecast-hourly -d P0DT3H

2. Fetch and save to the database all providers data
   for the same geographic location::

    weatherd bulk --save all-providers -y 5.59 -x -0.19

3. Fetch and save all available data by all providers for all locations,
   and parse JSON streams in the STDOUT by ``jq``::

    weatherd bulk --save everything | jq

4. Warm up the location meta cache (first thing you want to do in order to
   avoid maxing out on provider call limits)::

    weatherd bulk all-meta
