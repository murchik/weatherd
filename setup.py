#!/usr/bin/env python
import os
from setuptools import setup, find_packages

__doc__ = 'weatherd: collect and store weather forecasts'


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


readme = read('README.rst')
changelog = read('CHANGELOG.rst')
version = read('VERSION')

install_requires = [
    'click==8.1.3',
    'requests-cache==0.9.4',
    'wheel==0.37.1',
]

tests_require = [
    'pytest==7.1.2',
]

extras_require = {
    'testing': tests_require,
    'dev': [
        'neovim',
        'jedi',
        'ipython',
        'ipdb',
        'zest.releaser[recommended]',
    ],
    'db': {
        'SQLAlchemy==1.4.36',
        'alembic==1.7.7',
        'psycopg2-binary==2.9.3',
    },
    'ui': [
        'fastapi==0.78.0',
        'uvicorn[standard]==0.17.6',
        'jinja2==3.1.2',
    ],
}
extras_require['all'] = [v for s in extras_require.values() for v in s]

setup(
    name='weatherd',
    version=version,
    description=__doc__,
    long_description=readme + '\n\n' + changelog,
    author='murchik',
    author_email='murchik@mailbox.org',
    url='https://git.sr.ht/~murchik/weatherd',
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    tests_require=tests_require,
    extras_require=extras_require,
    entry_points={
        'console_scripts': [
            'weatherd=weatherd.cli:main',
            'weatherd-server=weatherd.server:main',
        ],
    },
    license='Proprietary',
    zip_safe=True,
    keywords='weatherd',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
)
