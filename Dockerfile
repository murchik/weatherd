# syntax=docker/dockerfile:experimental
FROM python:3.10.4-slim-bullseye

RUN apt-get update && apt-get install -y --no-install-recommends gcc make cron vim procps

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PATH=/root/.local/bin:$PATH

WORKDIR /app

COPY . /app

ARG PIP_INDEX_URL
ENV PIP_INDEX_URL=$PIP_INDEX_URL
RUN --mount=type=cache,target=/root/.cache/pip pip install --no-warn-script-location --user --default-timeout 90 -e '.[all]'
