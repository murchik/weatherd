class ProviderNotFound(Exception):
    pass


class DataFetchFailure(Exception):
    pass


class UnknownUnit(Exception):
    pass
