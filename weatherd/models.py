from sqlalchemy import (
    Column,
    ForeignKey,
    Boolean,
    Integer,
    String,
    DateTime,
    Float,
    Interval,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from .utils import convert_units


Base = declarative_base()


class Provider(Base):

    __tablename__ = 'providers'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(64), unique=True, nullable=False)
    implementation = Column(String(64), unique=True, nullable=False,
                            doc='Relative path to implementation class')

    forecasts = relationship('Forecast', back_populates='provider')


class Location(Base):

    __tablename__ = 'locations'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(64), unique=True, index=True, nullable=False)
    latitude = Column(Float, index=True, nullable=False)
    longitude = Column(Float, index=True, nullable=False)
    tz = Column(String(64), nullable=False, doc='Time zone')
    is_enabled = Column(Boolean, default=True, doc='Data collection enabled')

    forecasts = relationship('Forecast', back_populates='location')


class Metric(Base):

    __tablename__ = 'metrics'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(32), unique=True, index=True, nullable=False)
    unit = Column(String(32), nullable=False, doc='Unit of measurement')

    forecasts = relationship('Forecast', back_populates='metric')


# TODO: consider turning this into a time series storage
class Forecast(Base):

    __tablename__ = 'forecasts'

    id = Column(Integer, primary_key=True, index=True)
    fetched = Column(DateTime, index=True, nullable=False, doc='Fetch time')
    forecasted = Column(DateTime, index=True, nullable=False,
                        doc='Forecast time')
    duration = Column(Interval, nullable=False, doc='Forecast duration')
    value = Column(Float, index=True, nullable=False, doc='Metric value')

    metric_id = Column(Integer, ForeignKey('metrics.id'))
    location_id = Column(Integer, ForeignKey('locations.id'))
    provider_id = Column(Integer, ForeignKey('providers.id'))

    metric = relationship('Metric', back_populates='forecasts')
    location = relationship('Location', back_populates='forecasts')
    provider = relationship('Provider', back_populates='forecasts')


def save_forecast(db, forecast):
    provider = db.query(Provider).filter(
        Provider.name == forecast.provider,
    ).first()
    metric = db.query(Metric).filter(
        Metric.name == forecast.metric,
    ).first()
    location = db.query(Location).filter(
        Location.latitude == forecast.latitude,
        Location.longitude == forecast.longitude,
    ).first()

    value = convert_units(forecast.metric, forecast.unit, forecast.value)
    db_forecast = Forecast(
        fetched=forecast.fetched,
        forecasted=forecast.forecasted,
        duration=forecast.duration,
        value=value,
        provider_id=provider.id,
        metric_id=metric.id,
        location_id=location.id,
    )

    db.add(db_forecast)
    db.commit()
    db.refresh(db_forecast)
    return db_forecast


def get_all_locations(db):
    return db.query(Location).order_by(Location.id)
