import importlib

from ..exceptions import ProviderNotFound
from .base import ProviderBase
from .accuweather import AccuWeather
from .openweathermap import OpenWeatherMap
from .metno import MetNo
from .ignitia import Ignitia


__all__ = [
    'AccuWeather',
    'OpenWeatherMap',
    'MetNo',
    'Ignitia',
]


def get_all_implementations():
    for klass in ProviderBase.__subclasses__():
        module = klass.__module__.rsplit('.', 1)[-1]
        yield f'{module}.{klass.__name__}'


def get_implementation_class(script_class):
    try:
        module_name, class_name = script_class.rsplit('.', 1)
        module_path = f'weatherd.providers.{module_name}'
        module = importlib.import_module(module_path)
        return getattr(module, class_name)
    except (ValueError, ModuleNotFoundError, AttributeError):
        raise ProviderNotFound('Incorrect provider class string')
