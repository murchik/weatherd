import logging


log = logging.getLogger(__name__)


class ProviderBase:

    config_prefix = ''
    config = {}

    def __init__(self, config={}):
        self.load_config(config)

    def load_config(self, config):
        prefix = f'{self.config_prefix}_'
        for k, v in config.items():
            if k.lower().startswith(prefix):
                self.config[k.lower().replace(prefix, '')] = v
            else:
                log.debug('Unknown config key: %s', k)

    def fetch_all_forecasts(self, latitude, longitude):
        raise NotImplementedError

    def fetch_hourly_forecast(self, latitude, longitude, duration):
        raise NotImplementedError

    def fetch_daily_forecast(self, latitude, longitude, duration):
        raise NotImplementedError

    def fetch_location_meta(self, latitude, longitude):
        return {}
