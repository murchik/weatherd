import logging
import random
from datetime import datetime, timedelta
from urllib.parse import urljoin

import requests
from requests_cache import CachedSession

from weatherd.schemas import Forecast
from weatherd.exceptions import DataFetchFailure
from .base import ProviderBase


# structure: metric, value_retriever, unit_retriever
COMMON_METRICS = []

HOURLY_METRICS = [(
    'rain_probability',
    lambda x: x['RainProbability'],
    lambda x: '%'
), (
    'rain_amount',
    lambda x: x['Rain']['Value'],
    lambda x: x['Rain']['Unit'],
), (
    'temperature',
    lambda x: x['Temperature']['Value'],
    lambda x: x['Temperature']['Unit'],
), (
    'relative_humidity',
    lambda x: x['RelativeHumidity'],
    lambda x: '%',
)]

DAILY_METRICS = [(
    'rain_probability',
    lambda x: max(x['Day']['RainProbability'], x['Night']['RainProbability']),
    lambda x: '%'
), (
    'rain_amount',
    lambda x: x['Day']['Rain']['Value'] + x['Night']['Rain']['Value'],
    lambda x: x['Day']['Rain']['Unit'],
), (
    'temperature_max',
    lambda x: x['Temperature']['Maximum']['Value'],
    lambda x: x['Temperature']['Maximum']['Unit'],
), (
    'temperature_min',
    lambda x: x['Temperature']['Maximum']['Value'],
    lambda x: x['Temperature']['Minimum']['Unit'],
)]


DURATIONS = [
    ('hourly', '1hour', timedelta(hours=1)),
    ('hourly', '12hour', timedelta(hours=12)),
    ('hourly', '24hour', timedelta(hours=24)),
    ('hourly', '72hour', timedelta(hours=72)),
    ('hourly', '120hour', timedelta(hours=120)),
    ('daily', '1day', timedelta(days=1)),
    ('daily', '5day', timedelta(days=5)),
    ('daily', '10day', timedelta(days=10)),
    ('daily', '15day', timedelta(days=15)),
]

STANDARD_DAILY_DURATION = timedelta(days=5)
STANDARD_HOURLY_DURATION = timedelta(hours=12)


log = logging.getLogger(__name__)


def convert_duration(duration, kind, params=DURATIONS):
    param = None
    for param, delta in [(p, d) for k, p, d in params if k == kind]:
        if duration <= delta:
            return param

    log.debug('Duration limit %s reached: %s', param, duration)
    return param


class AccuWeather(ProviderBase):

    config_prefix = 'accuweather'
    api_url = 'https://dataservice.accuweather.com/'

    def fetch_all_forecasts(self, latitude, longitude):
        yield from self.fetch_hourly_forecast(latitude, longitude)
        yield from self.fetch_daily_forecast(latitude, longitude)

    def fetch_hourly_forecast(self, latitude, longitude,
                              duration=STANDARD_HOURLY_DURATION):
        data = self._fetch_forecast(latitude, longitude, duration, 'hourly')
        metrics = COMMON_METRICS + HOURLY_METRICS
        for item in data:
            for metric, value_retriever, unit_retriever in metrics:
                yield Forecast(**{
                    'metric': metric,
                    'value': value_retriever(item),
                    'unit': unit_retriever(item),
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': datetime.fromisoformat(item['DateTime']),
                    'duration': timedelta(hours=1),
                    'provider': self.__class__.__name__,
                })

    def fetch_daily_forecast(self, latitude, longitude,
                             duration=STANDARD_DAILY_DURATION):
        data = self._fetch_forecast(latitude, longitude, duration, 'daily')
        metrics = COMMON_METRICS + DAILY_METRICS
        for item in data['DailyForecasts']:
            for metric, value_retriever, unit_retriever in metrics:
                yield Forecast(**{
                    'metric': metric,
                    'value': value_retriever(item),
                    'unit': unit_retriever(item),
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': datetime.fromisoformat(item['Date']),
                    'duration': timedelta(days=1),
                    'provider': self.__class__.__name__,
                })

    def fetch_location_meta(self, latitude, longitude):
        url = '/locations/v1/cities/geoposition/search'
        params = {
            'q': f'{latitude},{longitude}',
            'language': 'en-us',
        }
        return self._fetch(url, params)

    def _fetch_forecast(self, latitude, longitude, duration, kind):
        meta = self.fetch_location_meta(latitude, longitude)
        location_id = self._get_location_id(meta)
        period = convert_duration(duration, kind)

        url = f'/forecasts/v1/{kind}/{period}/{location_id}'
        params = {
            'details': 'true',
            'metric': 'True',
        }
        return self._fetch(url, params, timedelta(minutes=30))

    def _fetch(self, url, params, cache=-1):
        slug = url.replace("/", "_")
        session = CachedSession(
            f'{self.config["cache_path"]}/accuweather{slug}',
            backend='filesystem',
            expire_after=cache,
        )
        resp = session.get(urljoin(self.api_url, url), params={
            'apikey': random.choices(self.config['api_keys'].split(',')),
            **params,
        })
        data = resp.json()
        if resp.status_code != 200:
            error = data.get('Message')
            raise DataFetchFailure(f'Unable to fetch: {error}')
        try:
            return resp.json()
        except requests.exceptions.JSONDecodeError as e:
            raise DataFetchFailure(f'Unable to parse response: {e}')

    def _get_location_id(self, meta):
        return meta['Key']
