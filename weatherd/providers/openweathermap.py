from datetime import datetime, timedelta
from urllib.parse import urljoin

from requests_cache import CachedSession

from weatherd.schemas import Forecast
from .base import ProviderBase

# structure: metric, value_retriever, unit_retriever
COMMON_METRICS = [(
    'rain_probability',
    lambda x: x['pop'],
    lambda x: 'decimal',
)]

HOURLY_METRICS = [(
    'rain_amount',
    lambda x: x.get('rain', {}).get('1h', 0),
    lambda x: 'mm',
), (
    'temperature',
    lambda x: x['temp'],
    lambda x: 'C',
), (
    'relative_humidity',
    lambda x: x['humidity'],
    lambda x: '%',
)]

DAILY_METRICS = [(
    'rain_amount',
    lambda x: x.get('rain', 0),
    lambda x: 'mm',
), (
    'temperature',
    lambda x: x['temp']['day'],
    lambda x: 'C',
), (
    'temperature_max',
    lambda x: x['temp']['max'],
    lambda x: 'C',
), (
    'temperature_min',
    lambda x: x['temp']['min'],
    lambda x: 'C',
)]

# XXX: not used by onecall, will be useful with separate daily/hourly fetches
STANDARD_DAILY_DURATION = timedelta(days=5)
STANDARD_HOURLY_DURATION = timedelta(hours=12)


class OpenWeatherMap(ProviderBase):

    config_prefix = 'openweathermap'
    api_url = 'https://api.openweathermap.org/data/2.5/'

    def fetch_all_forecasts(self, latitude, longitude):
        yield from self.fetch_hourly_forecast(latitude, longitude)
        yield from self.fetch_daily_forecast(latitude, longitude)

    def fetch_hourly_forecast(self, latitude, longitude,
                              duration=STANDARD_HOURLY_DURATION):
        data = self._fetch_onecall(latitude, longitude)
        metrics = COMMON_METRICS + HOURLY_METRICS
        for item in data['hourly']:
            for metric, value_retriever, unit_retriever in metrics:
                yield Forecast(**{
                    'metric': metric,
                    'value': value_retriever(item),
                    'unit': unit_retriever(item),
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': datetime.fromtimestamp(item['dt']),
                    'duration': timedelta(hours=1),
                    'provider': self.__class__.__name__,
                })

    def fetch_daily_forecast(self, latitude, longitude,
                             duration=STANDARD_DAILY_DURATION):
        data = self._fetch_onecall(latitude, longitude)
        metrics = COMMON_METRICS + DAILY_METRICS
        for item in data['daily']:
            for metric, value_retriever, unit_retriever in metrics:
                yield Forecast(**{
                    'metric': metric,
                    'value': value_retriever(item),
                    'unit': unit_retriever(item),
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': datetime.fromtimestamp(item['dt']),
                    'duration': timedelta(days=1),
                    'provider': self.__class__.__name__,
                })

    def _fetch_onecall(self, latitude, longitude):
        url = 'onecall'
        params = {
            'lat': latitude,
            'lon': longitude,
        }
        return self._fetch(url, params, timedelta(minutes=30))

    def _fetch(self, url, params, cache=-1):
        slug = url.replace("/", "_")
        session = CachedSession(
            f'{self.config["cache_path"]}/openweathermap{slug}',
            backend='filesystem',
            expire_after=cache,
        )
        resp = session.get(urljoin(self.api_url, url), params={
            'appid': self.config['api_key'],
            'units': 'metric',
            'lang': 'en',
            **params,
        })
        return resp.json()
