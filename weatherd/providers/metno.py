import logging
from collections import defaultdict
from datetime import datetime, time, timedelta
from urllib.parse import urljoin

from requests_cache import CachedSession

from weatherd.schemas import Forecast
from weatherd.utils import parse_zulu_time
from .base import ProviderBase

# structure: metric, value_retriever, unit_retriever
COMMON_METRICS = [(
    'rain_amount',
    lambda x: x.get('details', {})['precipitation_amount'],
    lambda x: 'mm',
), (
    'rain_probability',
    lambda x: x.get('details', {})['probability_of_precipitation'],
    lambda x: '%',
)]

HOURLY_METRICS = []

DAILY_METRICS = [(
    'temperature_max',
    lambda x: x.get('details', {})['air_temperature_max'],
    lambda x: 'C',
), (
    'temperature_min',
    lambda x: x.get('details', {})['air_temperature_min'],
    lambda x: 'C',
)]

DAILY_AGGREGATES = {
    'temperature_max': max,
    'temperature_min': min,
    'rain_amount': sum,
    'rain_probability': max,
}

# XXX: not used by /complete, may be useful with separate daily/hourly fetches
STANDARD_DAILY_DURATION = timedelta(days=5)
STANDARD_HOURLY_DURATION = timedelta(hours=12)


log = logging.getLogger(__name__)


class MetNo(ProviderBase):

    config_prefix = 'metno'
    api_url = 'https://api.met.no/weatherapi/locationforecast/2.0/'

    def fetch_all_forecasts(self, latitude, longitude):
        yield from self.fetch_hourly_forecast(latitude, longitude)
        yield from self.fetch_daily_forecast(latitude, longitude)

    def fetch_hourly_forecast(self, latitude, longitude,
                              duration=STANDARD_HOURLY_DURATION):
        data = self._fetch_complete(latitude, longitude)
        metrics = COMMON_METRICS + HOURLY_METRICS
        for ts in data['properties']['timeseries']:
            if 'next_1_hours' not in ts['data']:
                continue

            item = ts['data']['next_1_hours']
            forecasted = parse_zulu_time(ts['time'])
            for metric, value_retriever, unit_retriever in metrics:
                try:
                    value = value_retriever(item)
                except KeyError:
                    log.debug('No hourly %s at %s,%s: %s',
                              metric, latitude, longitude, forecasted)
                    continue
                yield Forecast(**{
                    'metric': metric,
                    'value': value,
                    'unit': unit_retriever(item),
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': forecasted,
                    'duration': timedelta(hours=1),
                    'provider': self.__class__.__name__,
                })

    def fetch_daily_forecast(self, latitude, longitude,
                             duration=STANDARD_DAILY_DURATION):
        DAY_QUARTERS = [time(0, 0), time(0, 6), time(0, 12), time(0, 18)]

        data = self._fetch_complete(latitude, longitude)
        metrics = COMMON_METRICS + DAILY_METRICS
        refined = defaultdict(lambda: defaultdict(lambda: {
            'unit': None,
            'values': [],
        }))

        for ts in data['properties']['timeseries']:
            if 'next_6_hours' not in ts['data']:
                continue

            # 6h forecasts are ubiquitous unlike 1h, and more complete than 12h
            item = ts['data']['next_6_hours']
            forecasted = parse_zulu_time(ts['time'])

            if forecasted.time() not in DAY_QUARTERS:
                continue

            for metric, value_retriever, unit_retriever in metrics:
                try:
                    value = value_retriever(item)
                except KeyError:
                    log.debug('No daily %s at %s,%s: %s',
                              metric, latitude, longitude, forecasted)
                    continue
                day = datetime.combine(forecasted.date(), time())
                refined[day][metric]['values'].append(value)
                refined[day][metric]['unit'] = unit_retriever(item)

        for forecasted, metrics in refined.items():
            for metric, item in metrics.items():
                value = DAILY_AGGREGATES[metric](item['values'])
                yield Forecast(**{
                    'metric': metric,
                    'value': value,
                    'unit': item['unit'],
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': forecasted,
                    'duration': timedelta(days=1),
                    'provider': self.__class__.__name__,
                })

    def _fetch_complete(self, latitude, longitude):
        url = 'complete'
        params = {
            'lat': latitude,
            'lon': longitude,
        }
        return self._fetch(url, params, timedelta(minutes=30))

    def _fetch(self, url, params, cache=-1):
        slug = url.replace("/", "_")
        session = CachedSession(
            f'{self.config["cache_path"]}/metno{slug}',
            backend='filesystem',
            expire_after=cache,
        )
        resp = session.get(urljoin(self.api_url, url), params=params, headers={
            'User-Agent': 'weatherd/0.1.0 gitlab.com/murchik/weatherd',
        })
        return resp.json()
