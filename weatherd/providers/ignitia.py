from datetime import date, datetime, timedelta

from requests_cache import CachedSession

from weatherd.schemas import Forecast
from weatherd.exceptions import DataFetchFailure
from .base import ProviderBase


# structure: metric, value_retriever, unit_retriever
COMMON_METRICS = []

HOURLY_METRICS = [(
    'rain_probability',
    lambda x: x['rainprob'],
    lambda x: '%',
), (
    'temperature',
    lambda x: x['temp'],
    lambda x: 'C',
), (
    'relative_humidity',
    lambda x: x['relhum'],
    lambda x: '%',
)]

DAILY_METRICS = [(
    'rain_probability',
    lambda x: x['drainprob'],
    lambda x: '%',
), (
    'rain_amount',
    lambda x: x.get('rainfall'),
    lambda x: 'mm',
), (
    'temperature_max',
    lambda x: x['maxtemp'],
    lambda x: 'C',
), (
    'temperature_min',
    lambda x: x['mintemp'],
    lambda x: 'C',
)]

STANDARD_DAILY_DURATION = timedelta(days=2)
STANDARD_HOURLY_DURATION = timedelta(days=2)

REGION_WEST_AFRICA = (3, -25, 27, 15)
REGION_SOUTH_AMERICA = (-54, -83, 12, -35)

REGIONS = [(
    REGION_WEST_AFRICA,
    'https://b2b.ignitia.se/api/iskaplusbr-mvp/forecast',
    'api_key_west_africa',
    # XXX: server or key issue
    # ), (
    # REGION_SOUTH_AMERICA,
    # 'https://br.api.b2b.ignitia.cloud/api/iskaplusbr-mvp/forecast',
    # 'api_key_south_america',
)]


def get_region_params(latitude, longitude):
    for area, url, key in REGIONS:
        if is_within_area((latitude, longitude), area):
            return url, key


def is_within_area(point, area):
    """
    Check if point coordinates are within area coordinates.

    >>> is_within_area((0, 10, 2, 20), (1, 15))
    True
    >>> is_within_area((0, 10, 2, 20), (3, 30))
    False
    """
    # TODO: support arbitrary polygonal area
    lat, lng = point
    lat1, lng1, lat2, lng2 = area
    if (lat1 < lat and lat < lat2):
        if (lng1 < lng and lng < lng2):
            return True
    return False


class Ignitia(ProviderBase):

    config_prefix = 'ignitia'

    def fetch_all_forecasts(self, latitude, longitude):
        yield from self.fetch_hourly_forecast(latitude, longitude)
        yield from self.fetch_daily_forecast(latitude, longitude)

    def fetch_hourly_forecast(self, latitude, longitude,
                              duration=STANDARD_HOURLY_DURATION):
        start = date.today()
        end = start + STANDARD_DAILY_DURATION
        data = self._fetch_forecasts(latitude, longitude, start, end)
        metrics = COMMON_METRICS + HOURLY_METRICS
        for forecasts in data.values():
            for item in forecasts['hourly']:
                for metric, value_retriever, unit_retriever in metrics:
                    yield Forecast(**{
                        'metric': metric,
                        'value': value_retriever(item),
                        'unit': unit_retriever(item),
                        'latitude': latitude,
                        'longitude': longitude,
                        'fetched': datetime.utcnow(),
                        'forecasted': datetime.fromisoformat(item['ts']),
                        'duration': timedelta(hours=1),
                        'provider': self.__class__.__name__,
                    })

    def fetch_daily_forecast(self, latitude, longitude,
                             duration=STANDARD_DAILY_DURATION):
        start = date.today()
        end = start + STANDARD_DAILY_DURATION
        data = self._fetch_forecasts(latitude, longitude, start, end)
        metrics = COMMON_METRICS + DAILY_METRICS
        for date_str, forecasts in data.items():
            item = forecasts['daily']
            for metric, value_retriever, unit_retriever in metrics:
                yield Forecast(**{
                    'metric': metric,
                    'value': value_retriever(item),
                    'unit': unit_retriever(item),
                    'latitude': latitude,
                    'longitude': longitude,
                    'fetched': datetime.utcnow(),
                    'forecasted': datetime.fromisoformat(date_str),
                    'duration': timedelta(days=1),
                    'provider': self.__class__.__name__,
                })

    def _fetch_forecasts(self, latitude, longitude, start, end):
        '''
        -H "Content-Type: application/json"
        -H "auth-key:Z6dEzvZdFVPMkbTEtA6ah5uD44PDrCgd"
        -X POST
        -d '{"lat":5.59, "lon":0.19, "date": "2022-07-23"}'
        https://b2b.ignitia.se/api/iskaplusbr-mvp/forecast
        '''
        try:
            url, key = get_region_params(latitude, longitude)
        except TypeError:
            raise DataFetchFailure(f'No region match: {latitude}, {longitude}')

        params = {
            'lat': latitude,
            'lon': longitude,
            'date_interval': {
                'start': start.isoformat(),
                'end': end.isoformat(),
            },
        }
        headers = {
            'Accept': 'application/json',
            'auth_key': self.config[key],
        }
        return self._fetch(url, params, headers, timedelta(minutes=30))

    def _fetch(self, url, params, headers, cache=-1):
        session = CachedSession(
            f'{self.config["cache_path"]}/ignitia_'
            f'{params["lat"]}_'
            f'{params["lon"]}_'
            f'{params["date_interval"]["start"]}_'
            f'{params["date_interval"]["end"]}',
            backend='filesystem',
            expire_after=cache,
        )
        resp = session.post(url, json=params, headers=headers, verify=False)
        return resp.json()
