from datetime import datetime, timezone
from .constants import METRICS, UNITS
from .exceptions import UnknownUnit


def convert_units(metric, unit, value):
    normal_unit = dict(METRICS).get(metric)
    if normal_unit == unit:
        return value

    converter = UNITS.get(metric, {}).get(unit, {}).get(normal_unit)
    if not converter:
        raise UnknownUnit

    return converter(value)


def parse_zulu_time(iso_time):
    """
    Parse Z-suffixed or regular ±<offset> time.

    >>> parse_zulu_time('2022-03-04T05:06:07Z')
    datetime(2022, 3, 4, 5, 6, 7, tzinfo=timezone.utc)
    >>> parse_zulu_time('2022-03-04T05:06:07Z+08:00')
    datetime(2022, 3, 4, 5, 6, 7, tzinfo=timezone(timedelta(hours=8)))
    """
    timestamp = datetime.fromisoformat(iso_time.replace('Z', ''))
    if iso_time.endswith('Z'):
        timestamp = timestamp.replace(tzinfo=timezone.utc)
    return timestamp
