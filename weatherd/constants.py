from .exceptions import UnknownUnit


PROJECT_PREFIX = 'WEATHERD'


LOCATIONS = [
    # Brazil
    ('Brasilia', -15.78, -47.93, 'America/Sao_Paulo'),
    ('Alta Floresta', -9.88, -56.07, 'America/San_Paulo'),
    ('Porto Alegre', -30.0, -51.23, 'America/San_Paulo'),
    ('Curitiba', -25.4, -49.27, 'America/San_Paulo'),
    ('Rio de Janeiro', -22.9, -43.18, 'America/San_Paulo'),
    ('Dourados', -22.2, -54.8, 'America/San_Paulo'),
    ('Canarana', -13.55, -52.27, 'America/San_Paulo'),
    ('Carolina', -7.33, -47.46, 'America/San_Paulo'),
    ('Conceicao do Araguaia', -8.26, -49.27, 'America/San_Paulo'),
    ('Floriano', -6.77, -43.03, 'America/San_Paulo'),
    ('Fortaleza', -3.73, -38.53, 'America/San_Paulo'),
    ('Nova Xavantina', -14.66, -52.35, 'America/San_Paulo'),
    ('Recife', -8.06, -34.92, 'America/San_Paulo'),
    ('Salvador', -12.93, -38.45, 'America/San_Paulo'),
    ('Sao Paulo', -23.37, -46.39, 'America/San_Paulo'),
    ('Belo Horizonte', -19.92, -43.98, 'America/San_Paulo'),
    ('Londrina', -23.32, -51.15, 'America/San_Paulo'),
    ('Uberlandia', -18.92, -48.27, 'America/San_Paulo'),

    # West Africa
    ('Accra', 5.59, -0.19, 'Africa/Abidjan'),
    ('Kumasi', 6.67, -1.63, 'Africa/Abidjan'),
    ('Tamale', 9.41, -0.82, 'Africa/Abidjan'),
    ('Abuja', 9.08, 7.41, 'Africa/Lagos'),
    ('Abidjan', 5.35, -4.00, 'Africa/Abidjan'),
    ('Yamoussoukro', 6.81, -5.27, 'Africa/Abidjan'),
    ('Korhogo', 9.46, -5.63, 'Africa/Abidjan'),
    ('Ouagadougou', 12.36, -1.52, 'Africa/Abidjan'),
    ('Bobo-Dioulasso', 11.18, -4.31, 'Africa/Abidjan'),
    ('Bamako', 12.64, -8.00, 'Africa/Abidjan'),
    ('Kano', 12.00, 8.56, 'Africa/lagos'),
    ('Niamey', 13.53, 2.12, 'Africa/Lagos'),
    ('Tambacounda', 13.77, -13.67, 'Africa/Abidjan'),
    ('Dakar', 14.72, -17.45, 'Africa/Abidjan'),
    ('Kankan', 10.38, -9.31, 'Africa/Abidjan'),
    ('Parakou', 9.33, 2.62, 'Africa/Lagos'),
    ('Maiduguri', 11.82, 13.15, 'Africa/Lagos'),
    ('Port Harcourt', 4.83, 7.00, 'Africa/Lagos'),
    ('Freetown', 8.46, -13.24, 'Africa/Abidjan'),
]


METRICS = [
    ('rain_probability', '%'),
    ('rain_amount', 'mm'),
    ('temperature', 'C'),
    ('temperature_max', 'C'),
    ('temperature_min', 'C'),
    ('relative_humidity', '%'),
]

UNITS = {
    'rain_probability': {
        'decimal': {
            '%': lambda x: x * 100,
        },
    },
}
