from datetime import datetime, timedelta

from pydantic import BaseModel


class Forecast(BaseModel):

    metric: str
    value: float
    unit: str
    latitude: float
    longitude: float
    fetched: datetime
    forecasted: datetime
    duration: timedelta
    provider: str

    class Config:
        orm_mode = True
