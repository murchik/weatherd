"""0002_init_data

Revision ID: 3336b3c7b7c4
Revises: e69104520207
Create Date: 2022-05-29 21:29:04.338266

"""
from alembic import op
import sqlalchemy as sa

from weatherd.constants import METRICS, LOCATIONS
from weatherd.models import Metric, Location, Provider
from weatherd.providers import get_all_implementations


# revision identifiers, used by Alembic.
revision = '3336b3c7b7c4'
down_revision = 'e69104520207'
branch_labels = None
depends_on = None

Session = sa.orm.sessionmaker()


def _add_metrics():
    bind = op.get_bind()
    session = Session(bind=bind)
    for name, unit in METRICS:
        metric = Metric(name=name, unit=unit)
        session.add(metric)
    session.commit()


def _add_locations():
    bind = op.get_bind()
    session = Session(bind=bind)
    for name, latitude, longitude, tz in LOCATIONS:
        location = Location(
            name=name, latitude=latitude, longitude=longitude, tz=tz)
        session.add(location)
    session.commit()


def _add_providers():
    bind = op.get_bind()
    session = Session(bind=bind)
    for implementation in get_all_implementations():
        name = implementation.split('.')[-1]
        provider = Provider(name=name, implementation=implementation)
        session.add(provider)
    session.commit()


def upgrade():
    _add_metrics()
    _add_locations()
    _add_providers()


def downgrade():
    pass
