"""0001_init

Revision ID: e69104520207
Revises:
Create Date: 2022-05-29 21:25:27.621395

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e69104520207'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('locations',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(length=64), nullable=False),
        sa.Column('latitude', sa.Float(), nullable=False),
        sa.Column('longitude', sa.Float(), nullable=False),
        sa.Column('tz', sa.String(length=64), nullable=False),
        sa.Column('is_enabled', sa.Boolean(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_locations_id'), 'locations', ['id'], unique=False)
    op.create_index(op.f('ix_locations_latitude'), 'locations', ['latitude'], unique=False)
    op.create_index(op.f('ix_locations_longitude'), 'locations', ['longitude'], unique=False)
    op.create_index(op.f('ix_locations_name'), 'locations', ['name'], unique=True)

    op.create_table('metrics',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(length=32), nullable=False),
        sa.Column('unit', sa.String(length=32), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_metrics_id'), 'metrics', ['id'], unique=False)
    op.create_index(op.f('ix_metrics_name'), 'metrics', ['name'], unique=True)

    op.create_table('providers',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(length=64), nullable=False),
        sa.Column('implementation', sa.String(length=64), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('implementation'),
        sa.UniqueConstraint('name')
    )
    op.create_index(op.f('ix_providers_id'), 'providers', ['id'], unique=False)

    op.create_table('forecasts',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('fetched', sa.DateTime(), nullable=False),
        sa.Column('forecasted', sa.DateTime(), nullable=False),
        sa.Column('duration', sa.Interval(), nullable=False),
        sa.Column('value', sa.Float(), nullable=False),
        sa.Column('metric_id', sa.Integer(), nullable=True),
        sa.Column('location_id', sa.Integer(), nullable=True),
        sa.Column('provider_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['location_id'], ['locations.id'], ),
        sa.ForeignKeyConstraint(['metric_id'], ['metrics.id'], ),
        sa.ForeignKeyConstraint(['provider_id'], ['providers.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_forecasts_fetched'), 'forecasts', ['fetched'], unique=False)
    op.create_index(op.f('ix_forecasts_forecasted'), 'forecasts', ['forecasted'], unique=False)
    op.create_index(op.f('ix_forecasts_id'), 'forecasts', ['id'], unique=False)
    op.create_index(op.f('ix_forecasts_value'), 'forecasts', ['value'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_forecasts_value'), table_name='forecasts')
    op.drop_index(op.f('ix_forecasts_id'), table_name='forecasts')
    op.drop_index(op.f('ix_forecasts_forecasted'), table_name='forecasts')
    op.drop_index(op.f('ix_forecasts_fetched'), table_name='forecasts')
    op.drop_table('forecasts')
    op.drop_index(op.f('ix_providers_id'), table_name='providers')
    op.drop_table('providers')
    op.drop_index(op.f('ix_metrics_name'), table_name='metrics')
    op.drop_index(op.f('ix_metrics_id'), table_name='metrics')
    op.drop_table('metrics')
    op.drop_index(op.f('ix_locations_name'), table_name='locations')
    op.drop_index(op.f('ix_locations_longitude'), table_name='locations')
    op.drop_index(op.f('ix_locations_latitude'), table_name='locations')
    op.drop_index(op.f('ix_locations_id'), table_name='locations')
    op.drop_table('locations')
