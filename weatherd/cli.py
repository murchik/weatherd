#!/usr/bin/env python

import logging
import json
from time import sleep
from datetime import timedelta
from random import randrange

import click as c
from pydantic import BaseModel

from .constants import PROJECT_PREFIX
from .db import get_db
from .models import save_forecast, get_all_locations
from .providers import (
    get_all_implementations,
    get_implementation_class,
)
from .exceptions import DataFetchFailure


log = logging.getLogger(__name__)


PROVIDERS = list(get_all_implementations())


class Duration(BaseModel):

    duration: timedelta


class DurationType(c.ParamType):

    name = 'duration'

    def convert(self, value, param, ctx):
        model = Duration(duration=value.upper())
        return model.duration


# Common
@c.group()
@c.option('-v', '--verbosity', default='info', help='Logging verbosity')
@c.option('-d', '--db-url', help='Database URL')
@c.option('-c', '--cache-path', default='data/cache', help='Cache path')
@c.option('-s', '--sleep-range-start', type=int, default=10,
          help='Sleep random interval range start')
@c.option('-z', '--sleep-range-stop', type=int, default=120,
          help='Sleep random interval range stop')
@c.option('--accuweather-api-keys', help='AccuWeather API keys')
@c.option('--openweathermap-api-key', help='OpenWeatherMap API key')
@c.option('--ignitia-api-key-west-africa', help='Ignitia West Africa API key')
@c.option('--ignitia-api-key-south-america',
          help='Ignitia South America API key')
@c.pass_obj
def cli(obj, verbosity, db_url, cache_path,
        sleep_range_start, sleep_range_stop,
        accuweather_api_keys, openweathermap_api_key,
        ignitia_api_key_west_africa, ignitia_api_key_south_america):
    logging.basicConfig(level=getattr(logging, verbosity.upper()))
    obj['accuweather_api_keys'] = accuweather_api_keys
    obj['openweathermap_api_key'] = openweathermap_api_key
    obj['ignitia_api_key_west_africa'] = ignitia_api_key_west_africa
    obj['ignitia_api_key_south_america'] = ignitia_api_key_south_america
    obj['db_url'] = db_url
    obj['cache_path'] = cache_path
    obj['sleep_range_start'] = sleep_range_start
    obj['sleep_range_stop'] = sleep_range_stop


# Fetch command group
@cli.group()
@c.pass_obj
@c.option('--save/--dry-run', default=False, help='Save results')
@c.option('-y', '--latitude', type=float, help='Latitude')
@c.option('-x', '--longitude', type=float, help='Longitude')
@c.option('-p', '--provider', type=c.Choice(PROVIDERS), help='Provider')
def fetch(obj, save, provider, latitude, longitude):
    c.secho(f'Fetching {provider} data for {latitude}, {longitude}:', err=True)
    obj['save'] = save
    obj['latitude'] = latitude
    obj['longitude'] = longitude
    obj['provider'] = provider


@fetch.command()
@c.pass_obj
def forecast_all(obj):
    """
    Fetch all forecasts.
    """
    prov = _get_provider(obj, obj['provider'])
    forecasts = prov.fetch_all_forecasts(obj['latitude'], obj['longitude'])
    _process_forecasts(forecasts, obj['save'], obj['db_url'])


@fetch.command()
@c.pass_obj
@c.option('-d', '--duration', type=DurationType(), default='P0DT3H')
def forecast_hourly(obj, duration):
    """
    Fetch hourly forecasts.
    """
    prov = _get_provider(obj, obj['provider'])
    forecasts = prov.fetch_hourly_forecast(
        obj['latitude'], obj['longitude'], duration)
    _process_forecasts(forecasts, obj['save'], obj['db_url'])


@fetch.command()
@c.pass_obj
@c.option('-d', '--duration', type=DurationType(), default='P1D')
def forecast_daily(obj, duration):
    """
    Fetch daily forecasts.
    """
    prov = _get_provider(obj, obj['provider'])
    forecasts = prov.fetch_daily_forecast(
        obj['latitude'], obj['longitude'], duration)
    _process_forecasts(forecasts, obj['save'], obj['db_url'])


@fetch.command()
@c.pass_obj
def location_meta(obj):
    """
    Fetch location meta.
    """
    prov = _get_provider(obj, obj['provider'])
    meta = prov.fetch_location_meta(obj['latitude'], obj['longitude'])
    print(json.dumps(meta))


# Bulk command group
@cli.group()
@c.pass_obj
@c.option('--save/--dry-run', default=False, help='Save results')
def bulk(obj, save):
    obj['save'] = save


@bulk.command()
@c.pass_obj
@c.option('-p', '--provider', type=c.Choice(PROVIDERS), help='Provider')
def all_locations(obj, provider):
    """
    Fetch forecasts for all locations.
    """
    db = next(get_db(obj['db_url']))
    for location in get_all_locations(db):
        prov = _get_provider(obj, provider)
        c.secho(f'Fetching forecasts for {location.name}', err=True)
        forecasts = prov.fetch_all_forecasts(
            location.latitude, location.longitude)
        _process_forecasts(forecasts, obj['save'], obj['db_url'])
        sleep(randrange(obj['sleep_range_start'], obj['sleep_range_stop']))


@bulk.command()
@c.pass_obj
@c.option('-y', '--latitude', type=float, help='Latitude')
@c.option('-x', '--longitude', type=float, help='Longitude')
def all_providers(obj, latitude, longitude):
    """
    Fetch location forecasts by all providers.
    """
    for provider in PROVIDERS:
        c.secho(f'Fetching {provider} data for given coordinates', err=True)
        prov = _get_provider(obj, provider)
        forecasts = prov.fetch_all_forecasts(latitude, longitude)
        _process_forecasts(forecasts, obj['save'], obj['db_url'])
        sleep(randrange(obj['sleep_range_start'], obj['sleep_range_stop']))


@bulk.command()
@c.pass_obj
def all_meta(obj):
    """
    Fetch all locations' meta.
    """
    db = next(get_db(obj['db_url']))
    for location in get_all_locations(db):
        c.secho(f'Fetching data for {location.name}', err=True, fg='magenta')
        for provider in PROVIDERS:
            c.secho(f'Fetching {provider} data for {location.name}', err=True)
            prov = _get_provider(obj, provider)
            meta = prov.fetch_location_meta(
                location.latitude, location.longitude)
            print(json.dumps(meta))


@bulk.command()
@c.pass_obj
def everything(obj):
    """
    Fetch location forecasts by all providers.
    """
    db = next(get_db(obj['db_url']))
    for location in get_all_locations(db):
        c.secho(f'Fetching data for {location.name}', err=True, fg='magenta')
        for provider in PROVIDERS:
            c.secho(f'Fetching {provider} data for {location.name}', err=True)
            prov = _get_provider(obj, provider)
            forecasts = prov.fetch_all_forecasts(
                location.latitude, location.longitude)
            _process_forecasts(forecasts, obj['save'], obj['db_url'])
            sleep(randrange(obj['sleep_range_start'], obj['sleep_range_stop']))


def _get_provider(obj, provider):
    prov_class = get_implementation_class(provider)
    opts = dict(obj)
    common_options = ['cache_path']
    for option in common_options:
        opts[f'{prov_class.config_prefix}_{option}'] = opts[option]
    return prov_class(opts)


def _process_forecasts(forecasts, save, db_url):
    db = next(get_db(db_url))
    try:
        for forecast in forecasts:
            if save:
                save_forecast(db, forecast)
                c.secho('Saved: ', fg='green', err=True, nl=False)
            print(forecast.json())
    except DataFetchFailure as e:
        log.error('Fetch error. %s', e)
        return
    c.secho('OK', fg='green', err=True)


def main():
    # pylint:disable=unexpected-keyword-arg,no-value-for-parameter
    cli(obj={}, auto_envvar_prefix=PROJECT_PREFIX)


if __name__ == '__main__':
    main()
