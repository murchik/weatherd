import os
import logging
from datetime import date, timedelta

from sqlalchemy.orm import Session
from fastapi import Request, APIRouter, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from weatherd.db import get_db
from weatherd.models import Forecast, Provider, Metric, Location


log = logging.getLogger('uvicorn')


router = APIRouter()
root = os.path.abspath(os.path.dirname(__file__))
media = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
templates = Jinja2Templates(directory=os.path.join(root, 'templates'))


@router.get('/', response_class=HTMLResponse)
def forecasts_page(request: Request):
    return templates.TemplateResponse('forecasts.html', {'request': request})


@router.get('/api/forecasts/')
def api_forecasts(
    request: Request,
    fetched_start: date | None = None,
    fetched_end: date | None = None,
    forecasted_start: date | None = None,
    forecasted_end: date | None = None,
    durations: str | None = None,
    providers: str | None = None,
    metrics: str | None = None,
    locations: str | None = None,
    db: Session = Depends(get_db),
):
    durs = [
        timedelta(seconds=int(x)) for x in durations.split(',')
    ] if durations else None
    provs = [int(x) for x in providers.split(',')] if providers else None
    metrs = [int(x) for x in metrics.split(',')] if metrics else None
    locs = [int(x) for x in locations.split(',')] if locations else None

    where = []
    if fetched_start:
        where.append(Forecast.fetched > fetched_start)
    if fetched_end:
        where.append(Forecast.fetched <= fetched_end)
    if forecasted_start:
        where.append(Forecast.forecasted > forecasted_start)
    if forecasted_end:
        where.append(Forecast.forecasted <= forecasted_end)
    if durations:
        where.append(Forecast.duration.in_(durs))
    if providers:
        where.append(Forecast.provider_id.in_(provs))
    if metrics:
        where.append(Forecast.metric_id.in_(metrs))
    if locations:
        where.append(Forecast.location_id.in_(locs))

    group = [
        Forecast.duration,
        Forecast.location_id,
        Forecast.metric_id,
        Forecast.provider_id,
        Forecast.forecasted,
    ]
    return db.query(*group, Forecast.fetched, Forecast.value) \
        .distinct(*group) \
        .where(*where) \
        .order_by(*group, Forecast.fetched.desc())[:1000]


@router.get('/api/durations/')
def api_durations(request: Request, db: Session = Depends(get_db)):
    # TODO: fetch unique durations
    return [3600, 3600 * 24]


@router.get('/api/providers/')
def api_providers(request: Request, db: Session = Depends(get_db)):
    return list(db.query(Provider))


@router.get('/api/metrics/')
def api_metrics(request: Request, db: Session = Depends(get_db)):
    return list(db.query(Metric))


@router.get('/api/locations/')
def api_locations(request: Request, db: Session = Depends(get_db)):
    return list(db.query(Location))
