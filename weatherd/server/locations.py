import os
import logging

from sqlalchemy.orm import Session
from fastapi import Request, APIRouter, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from weatherd.db import get_db
from weatherd.models import Location


log = logging.getLogger('uvicorn')


router = APIRouter()
root = os.path.abspath(os.path.dirname(__file__))
media = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
templates = Jinja2Templates(directory=os.path.join(root, 'templates'))


@router.get('/', response_class=HTMLResponse)
def forecasts_page(request: Request):
    return templates.TemplateResponse('locations.html', {'request': request})


@router.get('/api/locations/')
def api_locations(request: Request, db: Session = Depends(get_db)):
    return [x for x in db.query(Location)]
