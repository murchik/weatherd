import os

from fastapi import Request, APIRouter
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates


router = APIRouter()
root = os.path.abspath(os.path.dirname(__file__))
templates = Jinja2Templates(directory=os.path.join(root, 'templates'))


@router.get('/', response_class=HTMLResponse)
def root_handler(request: Request):
    return templates.TemplateResponse('root.html', {'request': request})
