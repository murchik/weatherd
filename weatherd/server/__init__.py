import os

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from .settings import get_settings
from . import root as root_routers
from . import forecasts
from . import locations
from . import monitoring


root = os.path.abspath(os.path.dirname(__file__))
app = FastAPI()
app.include_router(root_routers.router, tags=['root'])
app.include_router(forecasts.router, prefix='/forecasts', tags=['forecasts'])
app.include_router(locations.router, prefix='/locations', tags=['locations'])
app.include_router(monitoring.router, prefix='/monitoring', tags=['monitoring'])
app.mount('/static', StaticFiles(directory=os.path.join(root, 'static')),
          name='static')


def main():
    import uvicorn
    settings = get_settings()
    uvicorn.run(
        'weatherd.server:app',
        host=settings.server_host,
        port=settings.server_port,
        reload=True)


if __name__ == '__main__':
    main()
