from functools import lru_cache

from pydantic import BaseSettings


class Settings(BaseSettings):

    server_host: str = '0.0.0.0'
    server_port: int = 80

    class Config:
        env_prefix = 'weatherd_'


@lru_cache()
def get_settings():
    return Settings()
