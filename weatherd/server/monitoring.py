import logging

from sqlalchemy.orm import Session
from fastapi import Request, APIRouter, Depends, Path

from weatherd.db import get_db
from weatherd.models import Forecast, Provider


log = logging.getLogger('uvicorn')


router = APIRouter()


@router.get('/api/last-fetch/')
async def api_last_fetch(request: Request, db: Session = Depends(get_db)):
    last_fetch = get_last_fetch(db, [])
    return last_fetch.fetched


@router.get('/api/providers/')
async def api_providers(request: Request, db: Session = Depends(get_db)):
    return list(db.query(Provider))


@router.get('/api/providers/{provider_id}/last-fetch/')
async def api_providers_last_fetch(
        request: Request,
        provider_id: int = Path('Provider ID'),
        db: Session = Depends(get_db)
):
    filters = [Forecast.provider_id == provider_id]
    last_fetch = get_last_fetch(db, filters)
    return last_fetch.fetched


def get_last_fetch(db, filters):
    return db.query(Forecast).filter(*filters) \
        .order_by(Forecast.fetched.desc()).first()
