import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def get_engine(db_url=None):
    if not db_url:
        db_url = os.environ.get('WEATHERD_DB_URL')
    options = {
        'echo': bool(os.environ.get('WEATHERD_DEBUG', False)),
    }
    return create_engine(db_url, **options)


def get_session(db_url=None):
    engine = get_engine(db_url)
    return sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db(db_url=None):
    Session = get_session(db_url)
    db = Session()
    try:
        yield db
    finally:
        db.close()
